package com.rockitgroup.game.data.domain.service.league;

import com.rockitgroup.game.data.domain.model.enumeration.StatusEnum;
import com.rockitgroup.game.data.domain.model.league.LeagueRanking;
import com.rockitgroup.game.data.domain.model.league.LeagueRankingSnapshot;
import com.rockitgroup.game.data.domain.repository.league.LeagueRepository;
import com.rockitgroup.game.data.domain.service.cache.LeagueCacheService;
import com.rockitgroup.infrastructure.vitamin.common.exception.BadRequestException;
import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import com.rockitgroup.game.data.domain.model.league.League;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class LeagueService extends DefaultBaseService {

    @Autowired
    private LeagueRepository leagueRepository;

    @Autowired
    private LeagueRankingSnapshotService leagueRankingSnapshotService;

    @Autowired
    private LeagueCacheService leagueCacheService;


    @PostConstruct
    public void postConstruct() {
        this.repository = leagueRepository;
    }

    public List<League> findAllActiveLeagues(Long gameId) {
        return leagueRepository.findByGameIdAndStatus(gameId, StatusEnum.ACTIVE);
    }

    public League findLeague(Long gameId, Long leagueId) {
        Optional<League> result = leagueRepository.findById(leagueId);
        if (result.isPresent()) {
            League league = result.get();
            if (Objects.equals(league.getGameId(), gameId) && league.getStatus() == StatusEnum.ACTIVE) {
                return league;
            }
        }

        return null;
    }

    public void addLeagueRank(League league, Long accountId, Integer rank) {
        LeagueRankingSnapshot leagueRankingSnapshot = new LeagueRankingSnapshot();
        leagueRankingSnapshot.setCurrentRank(rank);

        leagueRankingSnapshot = (LeagueRankingSnapshot) leagueRankingSnapshotService.save(leagueRankingSnapshot);

        LeagueRanking leagueRanking = new LeagueRanking();
        leagueRanking.setLeague(league);
        leagueRanking.setAccountId(accountId);
        leagueRanking.setRankSnapshot(leagueRankingSnapshot);

        league.getRanks().add(leagueRanking);

        save(league);
    }

    public void updateLeagueRank(League league, Long accountId, Integer rank) {
        if (rank <= 0 || rank > league.getMaxTotalRank()) {
            throw new BadRequestException(String.format("Rank value is invalid. It need to be from 1 to %s", league.getMaxTotalRank()));
        }

        LeagueRanking leagueRanking = null;
        for (LeagueRanking element : league.getRanks()) {
            if (Objects.equals(element.getAccountId(), accountId) && Objects.equals(element.getLeague().getId(), league.getId())) {
                leagueRanking = element;
                break;
            }
        }

        if (leagueRanking == null) {
            addLeagueRank(league, accountId, rank);
        } else {
            LeagueRankingSnapshot leagueRankingSnapshot = new LeagueRankingSnapshot();
            leagueRankingSnapshot.setCurrentRank(rank);
            leagueRankingSnapshot.setPreviousRank(leagueRanking.getRankSnapshot().getCurrentRank());

            leagueRankingSnapshot = (LeagueRankingSnapshot) leagueRankingSnapshotService.save(leagueRankingSnapshot);

            leagueRanking.setRankSnapshot(leagueRankingSnapshot);

            save(league);
        }
    }

    @Override
    public void clearCache(Object o) {
        League league = (League) o;
        leagueCacheService.clearLeagueCache(league.getGameId(), league.getId());
    }

    @Override
    public void clearCaches(List list) {
        for (Object o : list) {
            League league = (League) o;
            leagueCacheService.clearLeagueCache(league.getGameId(), league.getId());
        }
    }

}
