package com.rockitgroup.game.data.domain.service.league;

import com.rockitgroup.game.data.domain.repository.league.LeagueRewardRepository;
import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class LeagueRewardService extends DefaultBaseService {

    @Autowired
    private LeagueRewardRepository leagueRewardRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = leagueRewardRepository;
    }
}
