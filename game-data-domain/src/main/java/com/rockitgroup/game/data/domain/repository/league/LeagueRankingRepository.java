package com.rockitgroup.game.data.domain.repository.league;

import com.rockitgroup.game.data.domain.model.league.LeagueRanking;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LeagueRankingRepository extends BaseRepository<LeagueRanking, Long> {

}
