package com.rockitgroup.game.data.domain.model.enumeration;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 1/5/19
 * Time: 8:08 PM
 */
public enum ResultEnum {

    PENDING, SUCCESS, FAIL
}
