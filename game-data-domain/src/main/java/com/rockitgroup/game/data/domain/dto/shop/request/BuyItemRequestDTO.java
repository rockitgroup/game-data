package com.rockitgroup.game.data.domain.dto.shop.request;

import com.rockitgroup.infrastructure.vitamin.common.dto.RequestDTO;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.Assert;

@Getter
@Setter
public class BuyItemRequestDTO extends RequestDTO {

    private Long itemId;

    @Override
    public void validate() throws IllegalArgumentException {
        Assert.notNull(itemId, "Missing parameter itemId");
    }
}
