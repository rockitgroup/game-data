package com.rockitgroup.game.data.domain.dto.mapper;

import com.rockitgroup.game.data.domain.dto.image.ImageDTO;
import com.rockitgroup.game.data.domain.dto.league.LeagueBasicInfoDTO;
import com.rockitgroup.game.data.domain.dto.league.LeagueDTO;
import com.rockitgroup.game.data.domain.dto.league.LeagueRankingDTO;
import com.rockitgroup.game.data.domain.dto.league.LeagueRewardDTO;
import com.rockitgroup.game.data.domain.model.league.League;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.DTOMapper;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.MapperFacadeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LeagueDTOMappingHandler implements DTOMapper.MappingHandler<League, LeagueDTO> {

    @Autowired
    private DTOMapper dtoMapper;

    @Override
    public LeagueDTO map(League model, Class<LeagueDTO> clazz) {
        LeagueDTO result = MapperFacadeFactory.create().map(model, LeagueDTO.class);

        if (model.getHigherLeague() != null) {
            result.setHigherLeague(dtoMapper.map(model.getHigherLeague(), LeagueBasicInfoDTO.class));
        }

        if (model.getLowerLeague() != null) {
            result.setLowerLeague(dtoMapper.map(model.getLowerLeague(), LeagueBasicInfoDTO.class));
        }

        if (model.getRanks() != null && !model.getRanks().isEmpty()) {
            result.setRanks(dtoMapper.map(model.getRanks(), LeagueRankingDTO.class));
        }

        if (model.getRewards() != null && !model.getRewards().isEmpty()) {
            result.setRewards(dtoMapper.map(model.getRewards(), LeagueRewardDTO.class));
        }

        if (model.getIconImage() != null) {
            result.setIconImage(dtoMapper.map(model.getIconImage(), ImageDTO.class));
        }

        return result;
    }
}
