package com.rockitgroup.game.data.domain.repository.image;

import com.rockitgroup.game.data.domain.model.image.Image;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends BaseRepository<Image, Long> {
}
