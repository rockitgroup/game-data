package com.rockitgroup.game.data.domain.repository.shop;

import com.rockitgroup.game.data.domain.model.shop.AccountItem;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountItemRepository extends BaseRepository<AccountItem, Long> {

    @Query(value = "SELECT * FROM account_items WHERE accountId=:accountId AND itemId=:itemId AND status=:status LIMIT 1", nativeQuery = true)
    AccountItem findOneByAccountIdAndItemIdAndStatus(@Param("accountId") Long accountId, @Param("itemId") Long itemId, @Param("status") String status);


}
