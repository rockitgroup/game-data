package com.rockitgroup.game.data.domain.dto.league;

import com.rockitgroup.game.data.domain.dto.image.ImageDTO;
import com.rockitgroup.infrastructure.vitamin.common.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class LeagueDTO extends BaseDTO{

    private String name;

    private LeagueBasicInfoDTO higherLeague;
    private LeagueBasicInfoDTO lowerLeague;

    private Integer maxTotalRank;

    private Integer promotionTopRange;
    private Integer delegationBottomRange;

    private List<LeagueRankingDTO> ranks;
    private List<LeagueRewardDTO> rewards;

    private ImageDTO iconImage;
}
