package com.rockitgroup.game.data.domain.model.league;

import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
public class LeagueRankingSnapshot extends Modifiable {

    private Integer currentRank;
    private Integer previousRank;
}
