package com.rockitgroup.game.data.domain.model.enumeration;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 12/21/18
 * Time: 1:33 AM
 */
public enum StatusEnum {

    ACTIVE, USED, DELETED
}
