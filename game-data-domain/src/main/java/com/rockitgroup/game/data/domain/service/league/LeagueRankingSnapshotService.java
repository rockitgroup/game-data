package com.rockitgroup.game.data.domain.service.league;

import com.rockitgroup.game.data.domain.repository.league.LeagueRankingSnapshotRepository;
import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class LeagueRankingSnapshotService extends DefaultBaseService {

    @Autowired
    private LeagueRankingSnapshotRepository leagueRankingSnapshotRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = leagueRankingSnapshotRepository;
    }
}
