package com.rockitgroup.game.data.domain.dto.mapper;

import com.rockitgroup.game.data.domain.dto.image.ImageDTO;
import com.rockitgroup.game.data.domain.dto.shop.ItemAttributeDTO;
import com.rockitgroup.game.data.domain.dto.shop.ItemDTO;
import com.rockitgroup.game.data.domain.model.shop.Item;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.DTOMapper;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.MapperFacadeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ItemDTOMappingHandler implements DTOMapper.MappingHandler<Item, ItemDTO> {

    @Autowired
    private DTOMapper dtoMapper;

    @Override
    public ItemDTO map(Item item, Class<ItemDTO> aClass) {
        ItemDTO itemDTO = MapperFacadeFactory.create().map(item, ItemDTO.class);

        if (item.getAttributes() != null && !item.getAttributes().isEmpty()) {
            itemDTO.setAttributes(dtoMapper.map(item.getAttributes(), ItemAttributeDTO.class));
        }

        if (item.getIconImage() != null) {
            itemDTO.setIconImage(dtoMapper.map(item.getIconImage(), ImageDTO.class));
        }

        return itemDTO;
    }
}
