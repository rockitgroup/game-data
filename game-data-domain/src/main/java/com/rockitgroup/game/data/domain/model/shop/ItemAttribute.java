package com.rockitgroup.game.data.domain.model.shop;

import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
public class ItemAttribute  extends Modifiable {

    @ManyToOne
    @JoinColumn
    private Item item;

    private String attributeKey;
    private String attributeValue;
}
