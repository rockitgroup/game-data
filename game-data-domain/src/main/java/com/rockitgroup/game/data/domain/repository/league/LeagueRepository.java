package com.rockitgroup.game.data.domain.repository.league;

import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import com.rockitgroup.game.data.domain.model.enumeration.StatusEnum;
import com.rockitgroup.game.data.domain.model.league.League;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LeagueRepository extends BaseRepository<League, Long> {

    List<League> findByGameIdAndStatus(Long gameId, StatusEnum status);
}


