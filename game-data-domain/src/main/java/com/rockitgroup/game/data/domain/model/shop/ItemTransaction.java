package com.rockitgroup.game.data.domain.model.shop;

import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import com.rockitgroup.game.data.domain.model.enumeration.ItemTransactionTypeEnum;
import com.rockitgroup.game.data.domain.model.enumeration.ResultEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@Getter
@Setter
public class ItemTransaction extends Modifiable{


    private Long itemId;
    private Long accountId;

    @Enumerated(EnumType.STRING)
    private ResultEnum result;

    @Enumerated(EnumType.STRING)
    private ItemTransactionTypeEnum transactionType;

    private String additionalInfo;

    private Integer totalAmountBefore;
    private Integer totalAmountAfter;
    private Integer requestAmount;

    public static ItemTransaction create(Long accountId, Long itemId, ItemTransactionTypeEnum transactionType) {
        ItemTransaction itemTransaction = new ItemTransaction();
        itemTransaction.setAccountId(accountId);
        itemTransaction.setItemId(itemId);
        itemTransaction.setResult(ResultEnum.PENDING);
        itemTransaction.setTransactionType(transactionType);
        return itemTransaction;
    }

}
