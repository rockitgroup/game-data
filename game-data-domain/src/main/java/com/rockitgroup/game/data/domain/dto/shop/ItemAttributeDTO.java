package com.rockitgroup.game.data.domain.dto.shop;

import com.rockitgroup.infrastructure.vitamin.common.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemAttributeDTO extends BaseDTO {

    private String attributeKey;
    private String attributeValue;
}
