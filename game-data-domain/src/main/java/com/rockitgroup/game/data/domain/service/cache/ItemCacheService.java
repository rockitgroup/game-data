package com.rockitgroup.game.data.domain.service.cache;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rockitgroup.game.data.domain.model.shop.Item;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.DTOMapper;
import com.rockitgroup.infrastructure.vitamin.common.redis.RedisServer;
import com.rockitgroup.infrastructure.vitamin.common.util.MapperUtils;
import com.rockitgroup.game.data.domain.dto.shop.ItemDTO;
import com.rockitgroup.game.data.domain.model.enumeration.StatusEnum;
import com.rockitgroup.game.data.domain.repository.shop.ItemRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class ItemCacheService {

    private static final String ALL_ACTIVE_ITEM_CACHE_KEY = "GAME_%s_ALL_ACTIVE_ITEM";

    private static final int ALL_ACTIVE_ITEM_CACHE_TIME = 60 * 60 * 24;

    @Autowired
    private RedisServer redisServer;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private DTOMapper dtoMapper;

    private ObjectMapper objectMapper = MapperUtils.getObjectMapper();

    public List<ItemDTO> getAllActiveItems(Long gameId) {
        String cacheKey = String.format(ALL_ACTIVE_ITEM_CACHE_KEY, gameId);
        String cacheValue = redisServer.get(cacheKey);
        if (cacheValue == null) {
            setAllActiveItemsCache(gameId);
            cacheValue = redisServer.get(cacheKey);
        }

        if (cacheValue != null) {
            try {
                return objectMapper.readValue(cacheValue, new TypeReference<List<ItemDTO>>() {});
            } catch (Exception e) {
                log.error(String.format("Failed to read value for cache key %s", cacheKey), e);
            }
        }

        return null;
    }

    public void clearAllActiveItemsCache(Long gameId) {
        String cacheKey = String.format(ALL_ACTIVE_ITEM_CACHE_KEY, gameId);
        redisServer.delete(cacheKey);
    }

    public void setAllActiveItemsCache(Long gameId) {
        List<Item> items =  itemRepository.findByGameIdAndStatus(gameId, StatusEnum.ACTIVE);
        if (items != null) {
            List<ItemDTO> itemDTOList = dtoMapper.map(items, ItemDTO.class);
            String cacheKey = String.format(ALL_ACTIVE_ITEM_CACHE_KEY, gameId);
            try {
                redisServer.add(cacheKey, objectMapper.writeValueAsString(itemDTOList), ALL_ACTIVE_ITEM_CACHE_TIME);
            } catch (Exception e) {
                log.error(String.format("Failed to add task type configuration rule %s to cache", cacheKey), e);
            }
        }
    }

    @Async
    public void setAllActiveItemsCacheAsync(Long gameId) {
        setAllActiveItemsCache(gameId);
    }
}
