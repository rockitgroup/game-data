package com.rockitgroup.game.data.domain.service.cache;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rockitgroup.game.data.domain.dto.league.LeagueDTO;
import com.rockitgroup.game.data.domain.repository.league.LeagueRepository;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.DTOMapper;
import com.rockitgroup.infrastructure.vitamin.common.redis.RedisServer;
import com.rockitgroup.infrastructure.vitamin.common.util.MapperUtils;
import com.rockitgroup.game.data.domain.model.enumeration.StatusEnum;
import com.rockitgroup.game.data.domain.model.league.League;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class LeagueCacheService {

    private static final String ALL_ACTIVE_LEAGUE_CACHE_KEY = "GAME_%s_ALL_ACTIVE_LEAGUE";

    private static final int ALL_ACTIVE_LEAGUE_CACHE_TIME = 60 * 60 * 24;

    private static final String LEAGUE_ID_CACHE_KEY = "GAME_%s_LEAGUE_%s";

    private static final int LEAGUE_ID_CACHE_TIME = 60 * 10;

    @Autowired
    private RedisServer redisServer;

    @Autowired
    private LeagueRepository leagueRepository;

    @Autowired
    private DTOMapper dtoMapper;

    private ObjectMapper objectMapper = MapperUtils.getObjectMapper();

    public LeagueDTO getLeague(Long gameId, Long leagueId) {
        String cacheKey = String.format(LEAGUE_ID_CACHE_KEY, gameId, leagueId);
        String cacheValue = redisServer.get(cacheKey);
        if (cacheValue == null) {
            setLeagueCache(gameId, leagueId);
            cacheValue = redisServer.get(cacheKey);
        }

        if (cacheValue != null) {
            try {
                return objectMapper.readValue(cacheValue, new TypeReference<List<LeagueDTO>>() {});
            } catch (Exception e) {
                log.error(String.format("Failed to read value for cache key %s", cacheKey), e);
            }
        }

        return null;
    }

    public List<LeagueDTO> getAllActiveLeagues(Long gameId) {
        String cacheKey = String.format(ALL_ACTIVE_LEAGUE_CACHE_KEY, gameId);
        String cacheValue = redisServer.get(cacheKey);
        if (cacheValue == null) {
            setAllActiveLeaguesCache(gameId);
            cacheValue = redisServer.get(cacheKey);
        }

        if (cacheValue != null) {
            try {
                return objectMapper.readValue(cacheValue, new TypeReference<List<LeagueDTO>>() {});
            } catch (Exception e) {
                log.error(String.format("Failed to read value for cache key %s", cacheKey), e);
            }
        }

        return null;
    }

    public void clearLeagueCache(Long gameId, Long leagueId) {
        String cacheKey = String.format(LEAGUE_ID_CACHE_KEY, gameId, leagueId);
        redisServer.delete(cacheKey);
    }

    public void setLeagueCache(Long gameId, Long leagueId) {
        Optional<League> result =  leagueRepository.findById(leagueId);
        if (result.isPresent()) {
            League league = result.get();
            if (Objects.equals(league.getGameId(), gameId) && league.getStatus() == StatusEnum.ACTIVE) {
                LeagueDTO leagueDTO = dtoMapper.map(league, LeagueDTO.class);
                String cacheKey = String.format(LEAGUE_ID_CACHE_KEY, gameId, leagueId);
                try {
                    redisServer.add(cacheKey, objectMapper.writeValueAsString(leagueDTO), LEAGUE_ID_CACHE_TIME);
                } catch (Exception e) {
                    log.error(String.format("Failed to add task type configuration rule %s to cache", cacheKey), e);
                }
            }
        }
    }


    public void clearAllActiveLeaguesCache(Long gameId) {
        String cacheKey = String.format(ALL_ACTIVE_LEAGUE_CACHE_KEY, gameId);
        redisServer.delete(cacheKey);
    }

    public void setAllActiveLeaguesCache(Long gameId) {
        List<League> leagues =  leagueRepository.findByGameIdAndStatus(gameId, StatusEnum.ACTIVE);
        if (leagues != null) {
            List<LeagueDTO> leagueDTOList = dtoMapper.map(leagues, LeagueDTO.class);
            String cacheKey = String.format(ALL_ACTIVE_LEAGUE_CACHE_KEY, gameId);
            try {
                redisServer.add(cacheKey, objectMapper.writeValueAsString(leagueDTOList), ALL_ACTIVE_LEAGUE_CACHE_TIME);
            } catch (Exception e) {
                log.error(String.format("Failed to add task type configuration rule %s to cache", cacheKey), e);
            }
        }
    }

    @Async
    public void setAllActiveLeaguedCacheAsync(Long gameId) {
        setAllActiveLeaguesCache(gameId);
    }

    @Async
    public void setLeagueCacheAsync(Long gameId, Long leagueId) {
        setLeagueCache(gameId, leagueId);
    }
}
