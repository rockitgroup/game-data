package com.rockitgroup.game.data.domain.model.league;

import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class LeagueRanking  extends Modifiable {

    @ManyToOne
    @JoinColumn
    private League league;

    private Long accountId;

    @OneToOne
    private LeagueRankingSnapshot rankSnapshot;

}
