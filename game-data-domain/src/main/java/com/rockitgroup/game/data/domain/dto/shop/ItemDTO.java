package com.rockitgroup.game.data.domain.dto.shop;

import com.rockitgroup.game.data.domain.dto.image.ImageDTO;
import com.rockitgroup.infrastructure.vitamin.common.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ItemDTO extends BaseDTO{

    private String name;
    private String description;
    private Integer buyPrice;
    private Integer sellPrice;
    private Integer quantity;
    private List<ItemAttributeDTO> attributes;
    private ImageDTO iconImage;
}
