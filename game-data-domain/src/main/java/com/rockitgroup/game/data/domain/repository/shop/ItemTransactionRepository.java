package com.rockitgroup.game.data.domain.repository.shop;

import com.rockitgroup.game.data.domain.model.shop.ItemTransaction;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemTransactionRepository extends BaseRepository<ItemTransaction, Long> {
}
