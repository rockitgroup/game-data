package com.rockitgroup.game.data.domain.service.league;

import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import com.rockitgroup.game.data.domain.repository.league.LeagueRankingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class LeagueRankingService extends DefaultBaseService {

    @Autowired
    private LeagueRankingRepository leagueRankingRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = leagueRankingRepository;
    }
}
