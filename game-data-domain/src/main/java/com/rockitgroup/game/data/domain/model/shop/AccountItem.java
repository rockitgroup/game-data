package com.rockitgroup.game.data.domain.model.shop;

import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import com.rockitgroup.game.data.domain.model.enumeration.StatusEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class AccountItem  extends Modifiable {

    private Long accountId;

    @OneToOne
    private Item item;

    @Enumerated(EnumType.STRING)
    private StatusEnum status;
}
