package com.rockitgroup.game.data.domain.dto.league;

import com.rockitgroup.infrastructure.vitamin.common.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LeagueRewardDTO extends BaseDTO{

    private Integer rankFrom;
    private Integer rankTo;
    private Integer rewardAmount;
}
