package com.rockitgroup.game.data.domain.service.shop;

import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import com.rockitgroup.game.data.domain.repository.shop.ItemTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class ItemTransactionService extends DefaultBaseService {

    @Autowired
    private ItemTransactionRepository itemTransactionRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = itemTransactionRepository;
    }
}
