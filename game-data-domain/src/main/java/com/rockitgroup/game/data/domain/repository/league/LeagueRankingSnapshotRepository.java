package com.rockitgroup.game.data.domain.repository.league;

import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import com.rockitgroup.game.data.domain.model.league.LeagueRankingSnapshot;
import org.springframework.stereotype.Repository;

@Repository
public interface LeagueRankingSnapshotRepository extends BaseRepository<LeagueRankingSnapshot, Long> {

}