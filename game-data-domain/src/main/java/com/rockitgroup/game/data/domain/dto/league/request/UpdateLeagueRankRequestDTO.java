package com.rockitgroup.game.data.domain.dto.league.request;

import com.rockitgroup.infrastructure.vitamin.common.dto.RequestDTO;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.Assert;

@Getter
@Setter
public class UpdateLeagueRankRequestDTO extends RequestDTO {

    private Long accountId;
    private Integer rank;

    @Override
    public void validate() throws IllegalArgumentException {
        Assert.notNull(accountId, "Missing parameter accountId");
        Assert.notNull(rank, "Missing parameter rank");
    }
}
