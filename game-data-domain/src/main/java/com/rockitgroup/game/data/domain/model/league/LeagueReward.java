package com.rockitgroup.game.data.domain.model.league;

import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
public class LeagueReward  extends Modifiable {

    @ManyToOne
    @JoinColumn
    private League league;

    private Integer rankFrom;
    private Integer rankTo;
    private Integer rewardAmount;
}
