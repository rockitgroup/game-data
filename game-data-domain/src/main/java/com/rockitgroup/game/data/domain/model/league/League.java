package com.rockitgroup.game.data.domain.model.league;


import com.rockitgroup.game.data.domain.model.enumeration.StatusEnum;
import com.rockitgroup.game.data.domain.model.image.Image;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class League extends Modifiable {

    private Long gameId;
    private String name;

    @OneToOne
    private League higherLeague;

    @OneToOne
    private League lowerLeague;

    private Integer maxTotalRank;

    private Integer promotionTopRange;
    private Integer delegationBottomRange;

    @OneToOne
    private Image iconImage;

    @Enumerated(EnumType.STRING)
    private StatusEnum status;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "league", fetch = FetchType.LAZY)
    private List<LeagueRanking> ranks;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "league", fetch = FetchType.LAZY)
    private List<LeagueReward> rewards;

}
