package com.rockitgroup.game.data.domain.model.enumeration;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 1/5/19
 * Time: 9:55 PM
 */
public enum ItemTransactionTypeEnum {

    BUY, SELL,
}
