package com.rockitgroup.game.data.domain.model.shop;

import com.rockitgroup.game.data.domain.model.image.Image;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import com.rockitgroup.game.data.domain.model.enumeration.StatusEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class Item  extends Modifiable {

    private String name;
    private String description;
    private Integer buyPrice;
    private Integer sellPrice;
    private Long gameId;

    @OneToOne
    private Image iconImage;

    @Transient
    private Integer quantity;

    @Enumerated(EnumType.STRING)
    private StatusEnum status;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "item", fetch = FetchType.LAZY)
    private List<ItemAttribute> attributes;
}
