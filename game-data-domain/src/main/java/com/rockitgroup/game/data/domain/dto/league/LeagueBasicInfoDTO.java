package com.rockitgroup.game.data.domain.dto.league;

import com.rockitgroup.infrastructure.vitamin.common.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LeagueBasicInfoDTO extends BaseDTO {

    private String name;
}
