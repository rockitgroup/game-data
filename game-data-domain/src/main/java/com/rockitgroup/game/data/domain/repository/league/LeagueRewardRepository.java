package com.rockitgroup.game.data.domain.repository.league;

import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import com.rockitgroup.game.data.domain.model.league.LeagueReward;
import org.springframework.stereotype.Repository;

@Repository
public interface LeagueRewardRepository extends BaseRepository<LeagueReward, Long> {
}
