package com.rockitgroup.game.data.domain.dto.image;

import com.rockitgroup.infrastructure.vitamin.common.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImageDTO extends BaseDTO {

    private String key;
    private String name;
}
