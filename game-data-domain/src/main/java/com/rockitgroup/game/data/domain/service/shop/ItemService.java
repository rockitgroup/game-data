package com.rockitgroup.game.data.domain.service.shop;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rockitgroup.game.data.domain.model.enumeration.AccountGameSettingKeyEnum;
import com.rockitgroup.game.data.domain.model.enumeration.ItemTransactionTypeEnum;
import com.rockitgroup.game.data.domain.model.enumeration.ResultEnum;
import com.rockitgroup.game.data.domain.model.enumeration.StatusEnum;
import com.rockitgroup.game.data.domain.model.shop.AccountItem;
import com.rockitgroup.game.data.domain.model.shop.Item;
import com.rockitgroup.game.data.domain.model.shop.ItemTransaction;
import com.rockitgroup.infrastructure.vitamin.common.dto.ResponseDTO;
import com.rockitgroup.infrastructure.vitamin.common.exception.BadRequestException;
import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import com.rockitgroup.infrastructure.vitamin.common.util.MapperUtils;
import com.rockitgroup.infrastructure.vitamin.common.util.WebUtils;
import com.rockitgroup.game.data.domain.repository.shop.AccountItemRepository;
import com.rockitgroup.game.data.domain.repository.shop.ItemRepository;
import com.rockitgroup.server.identity.management.domain.dto.account.AccountDTO;
import com.rockitgroup.server.identity.management.domain.dto.account.request.CreateAccountGameSettingsRequestDTO;
import com.rockitgroup.server.identity.management.domain.dto.game.AccountGameSettingDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class ItemService extends DefaultBaseService {

    @Value("${account.info.url}")
    private String accountInfoUrl;

    @Value("${account.update-setting.url}")
    private String accountUpdateSettingUrl;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private AccountItemRepository accountItemRepository;

    @Autowired
    private ItemTransactionService itemTransactionService;

    private ObjectMapper objectMapper = MapperUtils.getObjectMapper();

    @PostConstruct
    public void postConstruct() {
        this.repository = itemRepository;
    }

    public List<Item> getAccountItems(Long gameId, Long accountId) {
        return setItemQuantity(itemRepository.findAllAccountItems(gameId, accountId, StatusEnum.ACTIVE.name()));
    }

    public List<Item> findAllActiveItems(Long gameId) {
        return itemRepository.findByGameIdAndStatus(gameId, StatusEnum.ACTIVE);
    }

    private List<Item> setItemQuantity(List<Item> inputItem) {
        Map<Long, Item> itemCountMap = new HashMap<>();
        for (Item item : inputItem) {
            Item storedItem = item;
            if (itemCountMap.containsKey(item.getId())) {
                storedItem = itemCountMap.get(item.getId());
                storedItem.setQuantity(storedItem.getQuantity()+1);
            } else {
                storedItem.setQuantity(1);
            }
            itemCountMap.put(item.getId(), storedItem);
        }
        return new ArrayList<>(itemCountMap.values());
    }

    public Item useItem(Long gameId, Long itemId, Long accountId) {
        Optional<Item> item = itemRepository.findById(itemId);
        if (!item.isPresent() || !Objects.equals(item.get().getGameId(), gameId)) {
            throw new RuntimeException("Failed to find item");
        }

        AccountItem accountItem = accountItemRepository.findOneByAccountIdAndItemIdAndStatus(accountId, itemId, StatusEnum.ACTIVE.name());
        if (accountItem == null) {
            throw new BadRequestException("Account does not have this item");
        }

        accountItem.setStatus(StatusEnum.USED);
        accountItemRepository.saveAndFlush(accountItem);

        return item.get();
    }

    public AccountDTO buyItem(Long gameId, Long itemId, String authorization) throws Exception {
        Optional<Item> item = itemRepository.findById(itemId);
        if (!item.isPresent() || !Objects.equals(item.get().getGameId(), gameId)) {
            throw new RuntimeException("Failed to find item");
        }

        Long accountId = WebUtils.getAccountIdFromAuthorizationValue(authorization);

        ItemTransaction itemTransaction = ItemTransaction.create(accountId, itemId, ItemTransactionTypeEnum.BUY);

        ResponseDTO accountInfoResponse = WebUtils.authorizedGet(accountInfoUrl + "?gameId=" + Long.toString(gameId), authorization);

        if (accountInfoResponse.isSuccess()) {
            String accountInfoResult = MapperUtils.getObjectMapper().writeValueAsString(accountInfoResponse.getObject());
            AccountDTO accountDTO = objectMapper.readValue(accountInfoResult, AccountDTO.class);

            AccountGameSettingDTO moneyAmountSettings = null;
            List<AccountGameSettingDTO> newGameSettingDTOList = new ArrayList<>();
            List<AccountGameSettingDTO> gameSettingDTOList = accountDTO.getSettings();
            for (AccountGameSettingDTO accountGameSettingDTO : gameSettingDTOList) {
                if (Objects.equals(accountGameSettingDTO.getSettingKey(), AccountGameSettingKeyEnum.MONEY_AMOUNT.name())) {
                    moneyAmountSettings = accountGameSettingDTO;
                } else {
                    newGameSettingDTOList.add(accountGameSettingDTO);
                }
            }

            if (moneyAmountSettings == null || StringUtils.isEmpty(moneyAmountSettings.getSettingValue())) {
                throw new RuntimeException("Failed to retrieve account money amount");
            }

            Item buyItem = item.get();
            Integer currentMoneyAmount = Integer.parseInt(moneyAmountSettings.getSettingValue());
            if (currentMoneyAmount < buyItem.getBuyPrice()) {
                throw new BadRequestException("Do not have enough money");
            }

            moneyAmountSettings.setSettingValue(Integer.toString(currentMoneyAmount - buyItem.getBuyPrice()));
            newGameSettingDTOList.add(moneyAmountSettings);

            CreateAccountGameSettingsRequestDTO updateRequestDTO = new CreateAccountGameSettingsRequestDTO();
            updateRequestDTO.setGameSettings(newGameSettingDTOList);

            itemTransaction.setTotalAmountBefore(currentMoneyAmount);
            itemTransaction.setTotalAmountAfter(currentMoneyAmount - buyItem.getBuyPrice());
            itemTransaction.setRequestAmount(buyItem.getBuyPrice());

            ResponseDTO updateSettingResponse = WebUtils.authorizedPost(accountUpdateSettingUrl + "?gameId=" + Long.toString(gameId), authorization, updateRequestDTO);

            if (updateSettingResponse.isSuccess()) {
                AccountItem accountItem = new AccountItem();
                accountItem.setAccountId(accountDTO.getId());
                accountItem.setItem(item.get());
                accountItem.setStatus(StatusEnum.ACTIVE);

                accountItemRepository.saveAndFlush(accountItem);

                accountDTO.setSettings(newGameSettingDTOList);

                saveItemTransaction(itemTransaction, ResultEnum.SUCCESS, StringUtils.EMPTY);

                return accountDTO;
            } else {
                saveItemTransaction(itemTransaction, ResultEnum.FAIL, updateSettingResponse.getError().getMessage());
                WebUtils.handleAPIException(updateSettingResponse);
            }
        } else {
            saveItemTransaction(itemTransaction, ResultEnum.FAIL, accountInfoResponse.getError().getMessage());
            WebUtils.handleAPIException(accountInfoResponse);
        }

        return null;
    }

    public AccountDTO sellItem(Long gameId, Long itemId, String authorization) throws Exception {

        Optional<Item> item = itemRepository.findById(itemId);
        if (!item.isPresent() || !Objects.equals(item.get().getGameId(), gameId)) {
            throw new RuntimeException("Failed to find item");
        } else if (item.get().getSellPrice() == null){
            throw new BadRequestException("Item cannot be sold back");
        }

        Long accountId = WebUtils.getAccountIdFromAuthorizationValue(authorization);

        ItemTransaction itemTransaction = ItemTransaction.create(accountId, itemId, ItemTransactionTypeEnum.SELL);

        ResponseDTO accountInfoResponse = WebUtils.authorizedGet(accountInfoUrl + "?gameId=" + Long.toString(gameId), authorization);

        if (accountInfoResponse.isSuccess()) {
            String accountInfoResult = MapperUtils.getObjectMapper().writeValueAsString(accountInfoResponse.getObject());
            AccountDTO accountDTO = objectMapper.readValue(accountInfoResult, AccountDTO.class);

            AccountGameSettingDTO moneyAmountSettings = null;
            List<AccountGameSettingDTO> newGameSettingDTOList = new ArrayList<>();
            List<AccountGameSettingDTO> gameSettingDTOList = accountDTO.getSettings();
            for (AccountGameSettingDTO accountGameSettingDTO : gameSettingDTOList) {
                if (Objects.equals(accountGameSettingDTO.getSettingKey(), AccountGameSettingKeyEnum.MONEY_AMOUNT.name())) {
                    moneyAmountSettings = accountGameSettingDTO;
                } else {
                    newGameSettingDTOList.add(accountGameSettingDTO);
                }
            }

            if (moneyAmountSettings == null || StringUtils.isEmpty(moneyAmountSettings.getSettingValue())) {
                throw new RuntimeException("Failed to retrieve account money amount");
            }

            AccountItem accountItem = accountItemRepository.findOneByAccountIdAndItemIdAndStatus(accountDTO.getId(), itemId, StatusEnum.ACTIVE.name());
            if (accountItem == null) {
                throw new BadRequestException("Account does not have this item");
            }

            accountItem.setStatus(StatusEnum.DELETED);

            accountItemRepository.saveAndFlush(accountItem);

            Item sellItem = item.get();
            Integer currentMoneyAmount = Integer.parseInt(moneyAmountSettings.getSettingValue());

            moneyAmountSettings.setSettingValue(Integer.toString(currentMoneyAmount + sellItem.getSellPrice()));
            newGameSettingDTOList.add(moneyAmountSettings);

            CreateAccountGameSettingsRequestDTO updateRequestDTO = new CreateAccountGameSettingsRequestDTO();
            updateRequestDTO.setGameSettings(newGameSettingDTOList);

            itemTransaction.setTotalAmountBefore(currentMoneyAmount);
            itemTransaction.setTotalAmountAfter(currentMoneyAmount + sellItem.getSellPrice());
            itemTransaction.setRequestAmount(sellItem.getSellPrice());

            ResponseDTO updateSettingResponse = WebUtils.authorizedPost(accountUpdateSettingUrl + "?gameId=" + Long.toString(gameId), authorization, updateRequestDTO);

            if (updateSettingResponse.isSuccess()) {
                accountDTO.setSettings(newGameSettingDTOList);

                saveItemTransaction(itemTransaction, ResultEnum.SUCCESS, StringUtils.EMPTY);
                return accountDTO;
            } else {
                saveItemTransaction(itemTransaction, ResultEnum.FAIL, updateSettingResponse.getError().getMessage());
                WebUtils.handleAPIException(updateSettingResponse);
            }
        } else {
            saveItemTransaction(itemTransaction, ResultEnum.FAIL, accountInfoResponse.getError().getMessage());
            WebUtils.handleAPIException(accountInfoResponse);
        }

        return null;
    }


    private void saveItemTransaction(ItemTransaction itemTransaction, ResultEnum result, String additionalInfo) {
        itemTransaction.setResult(result);
        itemTransaction.setAdditionalInfo(additionalInfo);
        itemTransactionService.saveAsync(itemTransaction);
    }
}
