package com.rockitgroup.game.data.domain.repository.shop;

import com.rockitgroup.game.data.domain.model.enumeration.StatusEnum;
import com.rockitgroup.game.data.domain.model.shop.Item;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends BaseRepository<Item, Long> {

    List<Item> findByGameIdAndStatus(Long gameId, StatusEnum status);

    @Query(value = "SELECT * FROM items i INNER JOIN account_items ai ON ai.itemId = i.id WHERE ai.status=:status AND i.status=:status AND ai.accountId=:accountId AND i.gameId=:gameId", nativeQuery = true)
    List<Item> findAllAccountItems(@Param("gameId") Long gameId, @Param("accountId") Long accountId, @Param("status") String status);
}
