package com.rockitgroup.game.data.domain.service.image;

import com.rockitgroup.game.data.domain.repository.image.ImageRepository;
import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class ImageService extends DefaultBaseService {

    @Autowired
    private ImageRepository imageRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = imageRepository;
    }

}
