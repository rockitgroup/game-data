package com.rockitgroup.game.data.domain.model.image;

import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity
public class Image extends Modifiable {

    private String key;
    private String name;
}
