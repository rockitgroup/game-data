package com.rockitgroup.game.data.interfaces.controller;

import com.rockitgroup.infrastructure.vitamin.common.controller.BaseController;
import com.rockitgroup.infrastructure.vitamin.common.dto.ResponseDTO;
import com.rockitgroup.infrastructure.vitamin.common.exception.UnauthorizedException;
import com.rockitgroup.infrastructure.vitamin.common.redis.RedisServer;
import com.rockitgroup.infrastructure.vitamin.common.util.WebUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@Slf4j
public abstract class AppBaseController extends BaseController {

    private static final String AUTHENTICATE_REQUEST_CACHE_KEY = "AUTHENTICATE_REQUEST_%s";
    private static final int AUTHENTICATE_REQUEST_CACHE_TIME = 60; // Timeout in 1 minute

    @Autowired
    private RedisServer redisServer;

    @Value("${account.authentication.url}")
    private String accountAuthenticationUrl;

    protected Long authenticateRequest(HttpServletRequest request, Long gameId) throws Exception {
        String remoteAddress = request.getRemoteAddr();
        String authorizationValue = request.getHeader("Authorization");
        Long accountId = WebUtils.getAccountIdFromAuthorizationValue(authorizationValue);
        if (StringUtils.isEmpty(authorizationValue)) {
            throw new UnauthorizedException("Request is not authorized. Check header token");
        }
        String cacheKey = String.format(AUTHENTICATE_REQUEST_CACHE_KEY, remoteAddress);
        String cacheValue = redisServer.get(cacheKey);

        boolean isValidCache = isValidCacheValue(cacheValue, gameId, authorizationValue);
        if (isValidCache) {
            return accountId;
        } else {
            MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
            headers.add("Authorization", authorizationValue);
            headers.add("Content-Type", "application/json");

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            HttpEntity authRequest = new HttpEntity(headers);

            String url = accountAuthenticationUrl + "?gameId=" + Long.toString(gameId);

            ResponseDTO responseDTO = restTemplate.postForObject(url, authRequest, ResponseDTO.class);

            if (responseDTO.isSuccess()) {
                redisServer.add(cacheKey, getAuthorizationCacheValue(gameId, authorizationValue), AUTHENTICATE_REQUEST_CACHE_TIME);
                return accountId;
            } else {
                WebUtils.handleAPIException(responseDTO);
            }

            return null;
        }
    }

    private boolean isValidCacheValue(String cacheValue, Long gameId, String authorizationValue) {
        return StringUtils.isNotEmpty(cacheValue) && Objects.equals(getAuthorizationCacheValue(gameId, authorizationValue), cacheValue);
    }

    private String getAuthorizationCacheValue(Long gameId, String authorizationValue) {
        return Long.toString(gameId) + ":" + authorizationValue;
    }


}
