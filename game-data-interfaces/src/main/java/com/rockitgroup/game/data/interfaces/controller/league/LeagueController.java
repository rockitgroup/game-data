package com.rockitgroup.game.data.interfaces.controller.league;

import com.rockitgroup.game.data.interfaces.controller.AppBaseController;
import com.rockitgroup.infrastructure.vitamin.common.dto.ResponseDTO;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.DTOMapper;
import com.rockitgroup.infrastructure.vitamin.common.exception.NotFoundException;
import com.rockitgroup.game.data.domain.dto.league.LeagueDTO;
import com.rockitgroup.game.data.domain.dto.league.request.UpdateLeagueRankRequestDTO;
import com.rockitgroup.game.data.domain.model.league.League;
import com.rockitgroup.game.data.domain.service.cache.LeagueCacheService;
import com.rockitgroup.game.data.domain.service.league.LeagueService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/api/v1/league")
public class LeagueController extends AppBaseController {

    @Autowired
    private LeagueService leagueService;

    @Autowired
    private LeagueCacheService leagueCacheService;

    @Autowired
    private DTOMapper dtoMapper;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseDTO listAllLeague(HttpServletRequest servletRequest, @RequestParam(value = "gameId") Long gameId) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            authenticateRequest(servletRequest, gameId);
            List<LeagueDTO> allLeagueCache = leagueCacheService.getAllActiveLeagues(gameId);
            if (allLeagueCache == null || allLeagueCache.isEmpty()) {
                leagueCacheService.setAllActiveLeaguedCacheAsync(gameId);
                List<League> leagues = leagueService.findAllActiveLeagues(gameId);
                return generateSuccessResponse(responseDTO, dtoMapper.map(leagues, LeagueDTO.class));
            }
            return generateSuccessResponse(responseDTO, allLeagueCache);
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO, e);
        }
    }

    @RequestMapping(value = "/{leagueId}", method = RequestMethod.GET)
    public ResponseDTO getLeague(HttpServletRequest servletRequest,
                                 @PathVariable(value = "leagueId") Long leagueId,
                                 @RequestParam(value = "gameId") Long gameId) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            authenticateRequest(servletRequest, gameId);
            LeagueDTO leagueCache = leagueCacheService.getLeague(gameId, leagueId);
            if (leagueCache == null) {
                leagueCacheService.setLeagueCacheAsync(gameId, leagueId);
                League league = leagueService.findLeague(gameId, leagueId);
                return generateSuccessResponse(responseDTO, dtoMapper.map(league, LeagueDTO.class));
            }
            return generateSuccessResponse(responseDTO, leagueCache);
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO, e);
        }
    }

    @RequestMapping(value = "/{leagueId}/update-rank", method = RequestMethod.POST)
    public ResponseDTO updateLeagueRank(HttpServletRequest servletRequest,
                                     @PathVariable(value = "leagueId") Long leagueId,
                                     @RequestParam(value = "gameId") Long gameId,
                                        @RequestBody UpdateLeagueRankRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            requestDTO.validate();
            authenticateRequest(servletRequest, gameId);
            League league = leagueService.findLeague(gameId, leagueId);
            if (league == null) {
                throw new NotFoundException(String.format("Cannot find league with id %s", leagueId));
            }
            leagueService.updateLeagueRank(league, requestDTO.getAccountId(), requestDTO.getRank());
            return generateSuccessResponse(responseDTO);
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO, e);
        }
    }
}