package com.rockitgroup.game.data.interfaces.configuration;

import com.rockitgroup.infrastructure.vitamin.common.configuration.context.BaseApiServletContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
public class ApiServletContext extends BaseApiServletContext {

}
