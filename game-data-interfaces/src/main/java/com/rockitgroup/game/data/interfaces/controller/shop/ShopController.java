package com.rockitgroup.game.data.interfaces.controller.shop;

import com.rockitgroup.game.data.interfaces.controller.AppBaseController;
import com.rockitgroup.infrastructure.vitamin.common.dto.ResponseDTO;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.DTOMapper;
import com.rockitgroup.infrastructure.vitamin.common.exception.BadRequestException;
import com.rockitgroup.game.data.domain.dto.shop.ItemDTO;
import com.rockitgroup.game.data.domain.dto.shop.request.BuyItemRequestDTO;
import com.rockitgroup.game.data.domain.dto.shop.request.SellItemRequestDTO;
import com.rockitgroup.game.data.domain.dto.shop.request.UseItemRequestDTO;
import com.rockitgroup.game.data.domain.model.shop.Item;
import com.rockitgroup.game.data.domain.service.cache.ItemCacheService;
import com.rockitgroup.game.data.domain.service.shop.ItemService;
import com.rockitgroup.server.identity.management.domain.dto.account.AccountDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/api/v1/shop")
public class ShopController extends AppBaseController {

    @Autowired
    private ItemCacheService itemCacheService;

    @Autowired
    private ItemService itemService;

    @Autowired
    private DTOMapper dtoMapper;

    @RequestMapping(value = "/list-item", method = RequestMethod.GET)
    public ResponseDTO listAllItem(HttpServletRequest servletRequest, @RequestParam(value = "gameId") Long gameId) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            authenticateRequest(servletRequest, gameId);
            List<ItemDTO> allItemsCache = itemCacheService.getAllActiveItems(gameId);
            if (allItemsCache == null || allItemsCache.isEmpty()) {
                itemCacheService.setAllActiveItemsCacheAsync(gameId);
                List<Item> allItems = itemService.findAllActiveItems(gameId);
                return generateSuccessResponse(responseDTO, dtoMapper.map(allItems, ItemDTO.class));
            }

            return generateSuccessResponse(responseDTO, allItemsCache);
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO, e);
        }
    }

    @RequestMapping(value = "/account-item", method = RequestMethod.GET)
    public ResponseDTO getAccountItems(HttpServletRequest servletRequest, @RequestParam(value = "gameId") Long gameId) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            Long accountId = authenticateRequest(servletRequest, gameId);
            if (accountId == null) {
                throw new BadRequestException("Account Id is invalid");
            }
            List<Item> allItems = itemService.getAccountItems(gameId, accountId);
            return generateSuccessResponse(responseDTO, dtoMapper.map(allItems, ItemDTO.class));
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO, e);
        }
    }

    @RequestMapping(value = "/buy-item", method = RequestMethod.POST)
    public ResponseDTO buyItem(HttpServletRequest servletRequest, @RequestParam(value = "gameId") Long gameId, @RequestBody BuyItemRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            AccountDTO accountDTO = itemService.buyItem(gameId, requestDTO.getItemId(), servletRequest.getHeader("Authorization"));
            return generateSuccessResponse(responseDTO, accountDTO);
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO, e);
        }
    }


    @RequestMapping(value = "/sell-item", method = RequestMethod.POST)
    public ResponseDTO sellItem(HttpServletRequest servletRequest, @RequestParam(value = "gameId") Long gameId, @RequestBody SellItemRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            AccountDTO accountDTO = itemService.sellItem(gameId, requestDTO.getItemId(), servletRequest.getHeader("Authorization"));
            return generateSuccessResponse(responseDTO, accountDTO);
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO, e);
        }
    }

    @RequestMapping(value = "/use-item", method = RequestMethod.POST)
    public ResponseDTO useItem(HttpServletRequest servletRequest, @RequestParam(value = "gameId") Long gameId, @RequestBody UseItemRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            Long accountId = authenticateRequest(servletRequest, gameId);
            if (accountId == null) {
                throw new BadRequestException("Account Id is invalid");
            }
            Item item = itemService.useItem(requestDTO.getItemId(), requestDTO.getItemId(), accountId);
            return generateSuccessResponse(responseDTO, dtoMapper.map(item, ItemDTO.class));
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO, e);
        }
    }
}
