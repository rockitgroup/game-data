CREATE TABLE `item_transactions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `accountId` bigint(20) NOT NULL,
  `itemId` bigint(20) NOT NULL,
  `result` varchar(50) NOT NULL,
  `transactionType` varchar(50) NOT NULL,
  `additionalInfo` varchar(1000) DEFAULT NULL,
  `totalAmountBefore` int(10) DEFAULT NULL,
  `totalAmountAfter` int(10) DEFAULT NULL,
  `requestAmount` int(10) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;