CREATE TABLE `images` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;


ALTER TABLE `leagues`
  ADD COLUMN `iconImageId` bigint(20) DEFAULT NULL;

ALTER TABLE `items`
  ADD COLUMN `iconImageId` bigint(20) DEFAULT NULL;