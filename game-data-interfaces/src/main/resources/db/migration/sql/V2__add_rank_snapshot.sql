CREATE TABLE `league_ranking_snapshots` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `currentRank` int(10) NOT NULL,
  `previousRank` int(10) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `league_rankings`
  DROP COLUMN `currentRank`,
  DROP COLUMN `previousRank`,
  ADD COLUMN `rankSnapshotId` bigint(20) NOT NULL;